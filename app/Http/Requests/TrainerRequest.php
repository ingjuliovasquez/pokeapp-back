<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'last_name' => 'required',
            'email'     => 'required|unique:trainers,email'.$this->route('/api/trainers'),
            'password'  => 'required',
            'birthday'  => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => __('El nombre es requerido'),
            'last_name.required' => __('El apellido es requerido'),
            'email.required' => __('El correo es requerido'),
            'email.email' => __('El correo debe ser una dirección valida'),
            'password.required' => __('La contraseña es requerida'),
            'birthday.required' => __('Fecha de nacimiento es requerida')
        ];
    }
}
