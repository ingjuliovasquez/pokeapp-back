<?php

namespace App\Http\Controllers;

use App\Models\PokemonTeamTrainer;
use Illuminate\Http\Request;

class PokemonTeamTrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PokemonTeamTrainer  $pokemonTeamTrainer
     * @return \Illuminate\Http\Response
     */
    public function show(PokemonTeamTrainer $pokemonTeamTrainer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PokemonTeamTrainer  $pokemonTeamTrainer
     * @return \Illuminate\Http\Response
     */
    public function edit(PokemonTeamTrainer $pokemonTeamTrainer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PokemonTeamTrainer  $pokemonTeamTrainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PokemonTeamTrainer $pokemonTeamTrainer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PokemonTeamTrainer  $pokemonTeamTrainer
     * @return \Illuminate\Http\Response
     */
    public function destroy(PokemonTeamTrainer $pokemonTeamTrainer)
    {
        //
    }
}
