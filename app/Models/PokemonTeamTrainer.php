<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PokemonTeamTrainer extends Model
{
    use HasFactory;

    protected $fillable = ['pokemon_id', 'trainer_id', 'team_id'];

    public function pokemons()
    {
        return $this->hasMany(Pokemon::class);
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public function trainers()
    {
        return $this->hasMany(Trainer::class);
    }
}
