<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{
    use HasFactory;

    protected $fillable = ['region', 'register_period', 'ending_date'];

    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }
}
