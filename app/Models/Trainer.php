<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'last_name', 'email', 'birthday', 'team_id', 'tournament_id'];

    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function tournaments()
    {
        return $this->belongsToMany(Tournament::class);
    }
}
