<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function trainer()
    {
        return $this->hasOne(Trainer::class);
    }

    public function pokemons()
    {
        return $this->hasMany(Pokemon::class);
    }
}
