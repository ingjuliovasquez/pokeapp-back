<?php

namespace Database\Seeders;

use App\Models\Trainer;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class TrainerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'          => 'Entrenador',
                'last_name'     => 'Apellido Nuevo',
                'email'         => 'entrenadorq@pokemon.com',
                'password'      => Hash::make('password'),
                'birthday'      => Carbon::now()->subYear(20),
                'team_id'       => random_int(1, 3),
                'tournament_id' => random_int(1, 3),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ],
            [
                'name'          => 'Julio',
                'last_name'     => 'Vasquez',
                'email'         => 'devjulio@pokemon.com',
                'password'      => Hash::make('password'),
                'birthday'      => Carbon::now()->subYear(20),
                'team_id'       => random_int(1, 3),
                'tournament_id' => random_int(1, 3),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ],
            [
                'name'          => 'Ash',
                'last_name'     => 'Ketchum',
                'email'         => 'ashk@pokemon.com',
                'password'      => Hash::make('password'),
                'birthday'      => Carbon::now()->subYear(20),
                'team_id'       => random_int(1, 3),
                'tournament_id' => random_int(1, 3),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ],
        ];

        Trainer::insert($data);
    }
}
