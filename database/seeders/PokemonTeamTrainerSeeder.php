<?php

namespace Database\Seeders;

use App\Models\PokemonTeamTrainer;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PokemonTeamTrainerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'pokemon_id' => random_int(1, 5),
                'trainer_id' => random_int(1, 3),
                'team_id'    => 1
            ],
            [
                'pokemon_id' => random_int(1, 5),
                'trainer_id' => random_int(1, 3),
                'team_id'    => 2
            ],
            [
                'pokemon_id' => random_int(1, 5),
                'trainer_id' => random_int(1, 3),
                'team_id'    => 3
            ],
        ];

        PokemonTeamTrainer::insert($data);
    }
}
