<?php

namespace Database\Seeders;

use App\Models\Pokemon;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PokemonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'Bulbasur',
                'evolution' => 'Ivysur',
                'type' => 'Planta, Veneno',
                'moves' => 'Gruñido',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Charmander',
                'evolution' => 'Charmeleon',
                'type' => 'Fuego',
                'moves' => 'Dragoaliento',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Squirtle',
                'evolution' => 'Wartortle',
                'type' => 'Agua',
                'moves' => 'Hidropulso',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Caterpie',
                'evolution' => 'Metapod',
                'type' => 'Bicho',
                'moves' => 'Picadura',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Pikachu',
                'evolution' => 'Raichu',
                'type' => 'Electrico',
                'moves' => 'Impactrueno',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];

        Pokemon::insert($data);
    }
}
