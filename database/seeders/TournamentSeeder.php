<?php

namespace Database\Seeders;

use App\Models\Tournament;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TournamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'region' => 'Kanto',
                'register_period' => Carbon::now()->addMonth(random_int(3, 8)),
                'ending_date' => Carbon::now()->addYear(random_int(1, 6))
            ],
            [
                'region' => 'Johto',
                'register_period' => Carbon::now()->addMonth(random_int(3, 8)),
                'ending_date' => Carbon::now()->addYear(random_int(1, 6))
            ],
            [
                'region' => 'Hoenn',
                'register_period' => Carbon::now()->addMonth(random_int(3, 8)),
                'ending_date' => Carbon::now()->addYear(random_int(1, 6))
            ],
        ];

        Tournament::insert($data);
    }
}
