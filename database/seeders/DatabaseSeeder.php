<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Entrenador 1';
        $user->email = 'entrenador@pokemon.com';
        $user->password = Hash::make('password');
        $user->save();

        $this->call(PokemonSeeder::class);
        $this->call(TournamentSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(TrainerSeeder::class);
        $this->call(PokemonTeamTrainerSeeder::class);

    }
}
